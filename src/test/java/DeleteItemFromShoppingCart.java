import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.openqa.selenium.interactions.Action;

import javax.swing.*;
import java.util.ArrayList;


public class DeleteItemFromShoppingCart extends BaseSeleniumTestClass {
    @BeforeMethod
    public void beforeMethod() {
        goToUrl("http://54.37.125.177/");
    }

    @AfterMethod
    public void afterMethod() {
        driver.manage().deleteAllCookies();
    }

    @Test
    public void DeleteItemFromShoppingCart() {
        login("eugenekalafitskiy@gmail.com", "12345678");
//        Assert.assertTrue(driver.findElement(By.cssSelector(".ico-logout")).isDisplayed());  // Login to application and go to Computers --> Notebooks
//        Actions actions = new Actions(driver);
//        WebElement computers = driver.findElement(By.xpath("/html/body/div[6]/div[2]/ul[1]/li[1]/a"));
//        WebElement notebooks = driver.findElement(By.xpath("/html/body/div[6]/div[2]/ul[1]/li[1]/ul/li[2]/a"));
//        WebElement product1 = driver.findElement(By.xpath("/html/body/div[6]/div[3]/div/div[3]/div/div[2]/div[2]/div[2]/div/div/div[6]/div/div[2]/div[3]/div[2]"));
//        WebElement product2 = driver.findElement(By.xpath("/html/body/div[6]/div[3]/div/div[3]/div/div[2]/div[2]/div[2]/div/div/div[3]/div/div[2]/div[3]/div[2]/button[1]"));
//        WebElement product3 = driver.findElement(By.xpath("/html/body/div[6]/div[3]/div/div[3]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div[2]/div[3]/div[2]/button[1]"));
//        Action moveToComputers = actions
//                .moveToElement(computers)
//                .pause(1000)
//                .moveToElement(notebooks)
//                .click()
//                .moveToElement(product1)
//                .click()
//                .moveToElement(product2)
//                .click()
//                .moveToElement(product3)
//                .click()
//                .build();
//Add 2-3 different items to cart (except Apple)
driver.findElement(By.xpath("/html/body/div[6]/div[2]/ul[1]/li[1]/a")).click();

driver.findElement(By.xpath("/html/body/div[6]/div[3]/div/div[3]/div/div[2]/div[1]/div/div[2]/div/div")).click();

driver.findElement(By.xpath("/html/body/div[6]/div[3]/div/div[3]/div/div[2]/div[2]/div[2]/div/div/div[6]/div/div[2]/div[3]/div[2]"));

driver.findElement(By.xpath("/html/body/div[6]/div[3]/div/div[3]/div/div[2]/div[2]/div[2]/div/div/div[3]/div/div[2]/div[3]/div[2]/button[1]"));
driver.findElement(By.xpath("/html/body/div[6]/div[3]/div/div[3]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div[2]/div[3]/div[2]/button[1]"));

        driver.findElement(By.xpath("/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[4]/a")).click(); // Go to cart

        driver.findElement(By.xpath("//*[@id=\"shopping-cart-form\"]/div[2]/div[1]/button[1]")).click(); // Delete 1 item and save cart

        String str =    driver.findElement(By.xpath("//*[@id=\"topcartlink\"]/a/span[2]")).getText();
//        char[] x = str.toCharArray();
//        char result = x[1];
//        Assert.assertEquals(result, 2);
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"topcartlink\"]/a/span[2]")).getText(), ("(2)"));
    }


    private void login(String userName, String password) {
        driver.findElement(By.cssSelector(".ico-login")).click();
        driver.findElement(By.id("Email")).sendKeys(userName);
        driver.findElement(By.id("Password")).sendKeys(password);
        driver.findElement(By.cssSelector(".button-1.login-button")).click();


    }
}
