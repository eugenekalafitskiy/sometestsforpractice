import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class BadTest extends BaseSeleniumTestClass{
    @AfterMethod
    public void afterMethod() {
        driver.manage().deleteAllCookies();
    }

    @Test
    public void BadTest(){
        System.setProperty("webdriver.chrome.driver", "C:\\Java\\EKalafitskiyproject\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://54.37.125.177/");
        driver.findElement(By.cssSelector(".ico-login")).click();
        driver.findElement(By.id("Email")).sendKeys("eugenekalafitskiy@gmail.com");
        driver.findElement(By.id("Password")).sendKeys("12345678");
        driver.findElement(By.cssSelector(".button-1.login-button")).click();
        Assert.assertTrue(driver.findElement(By.cssSelector(".ico-logout")).isDisplayed());
    }
    @Test
    public void BadLogoutTest(){
        System.setProperty("webdriver.chrome.driver", "C:\\Java\\EKalafitskiyproject\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://54.37.125.177/");
        driver.findElement(By.cssSelector(".ico-login")).click();
        driver.findElement(By.id("Email")).sendKeys("eugenekalafitskiy@gmail.com");
        driver.findElement(By.id("Password")).sendKeys("12345678");
        driver.findElement(By.cssSelector(".button-1.login-button")).click();
        Assert.assertTrue(driver.findElement(By.cssSelector(".ico-logout")).isDisplayed());
        driver.findElement(By.cssSelector(".ico-logout")).click();
        Assert.assertTrue(driver.findElement(By.cssSelector(".ico-login")).isDisplayed());
    }

}
