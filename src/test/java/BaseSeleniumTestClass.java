import Driver.WebDriverFactory;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.PropertiesReader;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class BaseSeleniumTestClass {
    protected WebDriver driver = null;

    @BeforeSuite
    public void beforeSuite() {
        driver = WebDriverFactory.initDriver();
        driver.manage().window().maximize();
        File downloadDir = new File(PropertiesReader.getInstance().getProperty("downloadFolder"));
        if (downloadDir.exists()) {
            try {
                FileUtils.cleanDirectory(downloadDir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @AfterSuite
    public void afterSuite() {
        if (driver != null) {
            driver.quit();
        }
    }

    public void goToUrl(String url) {
        driver.navigate().to(url);
    }

    public void pause(long msec) {
        try {
            Thread.sleep(msec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result) {
        if (!result.isSuccess()) {
            File screenshotAs = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            File file = new File(result.getName() + "_" + new Date().getTime() + ".png");
            try {
                FileUtils.copyFile(screenshotAs, file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}