import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class BadLoginTest extends BaseSeleniumTestClass{
    @BeforeMethod
    public void beforeMethod() {
        goToUrl("http://54.37.125.177/");
    }

    @AfterMethod
    public void afterMethod(){
        driver.manage().deleteAllCookies();
    }

    @Test
    public void BadLoginTest(){
        driver.findElement(By.cssSelector(".ico-login")).click();
        driver.findElement(By.id("Email")).sendKeys("eugenekalafitskiy@gmail.com");
        driver.findElement(By.id("Password")).sendKeys("12345678");
        driver.findElement(By.cssSelector(".button-1.login-button")).click();
        Assert.assertTrue(driver.findElement(By.cssSelector(".ico-logout")).isDisplayed());
    }

    @Test
    public void BadLogoutTest(){
        driver.findElement(By.cssSelector(".ico-login")).click();
        driver.findElement(By.id("Email")).sendKeys("eugenekalafitskiy@gmail.com");
        driver.findElement(By.id("Password")).sendKeys("12345678");
        driver.findElement(By.cssSelector(".button-1.login-button")).click();
        Assert.assertTrue(driver.findElement(By.cssSelector(".ico-logout")).isDisplayed());
        driver.findElement(By.cssSelector(".ico-logout")).click();
        Assert.assertTrue(driver.findElement(By.cssSelector(".ico-login")).isDisplayed());
    }
}
