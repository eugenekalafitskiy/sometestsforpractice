package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {
    private static PropertiesReader instance = null;
    private Properties properties;

    private PropertiesReader() {
        properties = new Properties();
        try {
//             String env = System.getProperty("envType").toUpperCase();
//              String filePath = String.format("src/test/resources/appLOCAL.properties", env);
            properties.load(new FileInputStream("src/test/resources/appREMOTE.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static PropertiesReader getInstance() {
        if (instance == null) {
            instance = new PropertiesReader();
        }
        return instance;
    }

    public String getProperty(String propertyName) {
        return properties.getProperty(propertyName);
    }

}
