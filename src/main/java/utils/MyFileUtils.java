package utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.RandomStringUtils;

import java.io.File;
import java.io.IOException;

public class MyFileUtils {
    public static File createDuplicate(File template) {
        File parent = template.getParentFile();
        String name = template.getName();

        String baseName = FilenameUtils.getBaseName(name);
        String extension = FilenameUtils.getExtension(name);
        String nameOfCopy = baseName + "_"
                + RandomStringUtils.randomAlphanumeric(6)
                + "." + extension;
        File copy = new File(parent, nameOfCopy);

        try {
            FileUtils.copyFile(template, copy);
        } catch (IOException e) {
            e.printStackTrace();
        }
        copy.deleteOnExit();
        return copy;
    }
}
