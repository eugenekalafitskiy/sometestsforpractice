package Driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import utils.PropertiesReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class WebDriverFactory {
    public static WebDriver initDriver(Browsers browser) {
        switch (browser) {
            case EDGE: {
                WebDriverManager.edgedriver().setup();
                return new EdgeDriver();
            }
            case CHROME: {
                WebDriverManager.chromedriver().setup();

                File downloadDir = new File(PropertiesReader.getInstance().getProperty("downloadFolder"));

                Map<String, Object> prefsMap = new HashMap<>();
                prefsMap.put("profile.default_content_settings.popups", 0);
                prefsMap.put("download.default_directory", downloadDir.getAbsolutePath());

                ChromeOptions option = new ChromeOptions();
                option.setExperimentalOption("prefs", prefsMap);

                return new ChromeDriver(option);
            }
            case FIREFOX: {
                WebDriverManager.firefoxdriver().setup();
                return new FirefoxDriver();
            }
        }
        return null;
    }

    public static WebDriver initDriver() {
        String browserName = System.getProperty("browserName", "chrome");
        try {
            return initDriver(Browsers.valueOf(browserName.toUpperCase()));
        }catch (IllegalArgumentException e){
            System.err.println("This browser is not supported!!!");
            System.exit(-1);
        }
        return null;
    }
}
